# bird_bot

Fun telegram bot for friends

Environment variables:
BIRD_BOT_TOKEN # telegram bot token
BIRD_GOOGLE_API_KEY # google cloud search api (random image search feature)
BIRD_GOOGLE_CSE_ID # google custom search engine (random image search feature)
BIRD_YANDEX_PREDICTOR_KEY # yandex predictor service (bird predict feature)
