#on vps
locale-gen ru_RU.UTF-8
dpkg-reconfigure locales

GOOGLE API IMAGE SEARCH URL

https://www.googleapis.com/customsearch/v1?
    q={searchTerms}&
    num={count?}&
    start={startIndex?}&
    lr={language?}&
    safe={safe?}&
    cx=012618794295774294121:dnqqhon2jaw&
    sort={sort?}&
    filter={filter?}&
    gl={gl?}&
    cr={cr?}&
    googlehost={googleHost?}&
    c2coff={disableCnTwTranslation?}&
    hq={hq?}&
    hl={hl?}&
    siteSearch={siteSearch?}&
    siteSearchFilter={siteSearchFilter?}&
    exactTerms={exactTerms?}&
    excludeTerms={excludeTerms?}&
    linkSite={linkSite?}&
    orTerms={orTerms?}&
    relatedSite={relatedSite?}&
    dateRestrict={dateRestrict?}&
    lowRange={lowRange?}&
    highRange={highRange?}&
    searchType={searchType}&
    fileType={fileType?}&
    rights={rights?}&
    imgSize={imgSize?}&
    imgType={imgType?}&
    imgColorType={imgColorType?}&
    imgDominantColor={imgDominantColor?}&
    alt=json

https://developers.google.com/custom-search/v1/reference/rest/v1/cse/list
https://stackoverflow.com/questions/34035422/google-image-search-says-api-no-longer-available

LIMIT EXCEEDED RESPONSE

{
  "error": {
    "code": 403,
    "message": "Request throttled due to daily limit being reached.",
    "errors": [
      {
        "message": "Request throttled due to daily limit being reached.",
        "domain": "usageLimits",
        "reason": "dailyLimitExceeded"
      }
    ],
    "status": "RESOURCE_EXHAUSTED"
  }
}