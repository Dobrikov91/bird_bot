import random
import logging
from datetime import datetime

from bird_bot import logger_config

logger = logging.getLogger(__name__)

def sleep(x):
    return 'Z' + 'z' * random.randrange(3, x, 2)

def tweet(x):
    return 'Чирик! ' * random.randrange(1, x, 1)

def add_name(text, name, perc):
    if random.randrange(101) <= perc and name:
        return name
    return ''

def reply(text, name, surname):
    now = datetime.now()
    if now.hour > 22 or now.hour < 8:
        return ('Send', sleep(15))
    return ('Send', '{}{} {} {}'.format(tweet(3), add_name(text, name, 100), add_name(text, surname, 10), tweet(4)))
