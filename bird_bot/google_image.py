import os
import random
import logging
import re
import requests
import json

from bird_bot import logger_config

logger = logging.getLogger(__name__)

selfnames = r'\s*(self|себя|пиздун)'
colors = ('black', 'blue', 'brown', 'gray', 'green', 'pink', 'purple', 'teal', 'white', 'yellow')

def google_request_url(query):
    url = 'https://www.googleapis.com/customsearch/v1?q={}&num={}&cx={}&key={}&searchType={}&safe={}&imgDominantColor={}&alt=json'.format(
        query,
        10,
        os.getenv('BIRD_GOOGLE_CSE_ID'),
        os.getenv('BIRD_GOOGLE_API_KEY'),
        'IMAGE',
        'OFF',
        random.choice(colors),
    )
    logger.info(url)
    return url

def google_result(query):
    resp = requests.get(google_request_url(query))
    if resp.status_code != 200:
        if resp.status_code == 403 and 'dailyLimitExceeded' in resp.text:
            return 'На сегодня лимит вышел, заходи попозже'
        return 'Wrong status code {}'.format(resp.status_code)

    logger.info(resp.text)
    data = json.loads(resp.text)
    return [item['link'] for item in data['items']]


def get_random_image(text):
    if not os.getenv('BIRD_GOOGLE_CSE_ID') or not os.getenv('BIRD_GOOGLE_API_KEY'):
        return 'Reply', 'Фича не работает и не будет. Вот'

    logger.info('Trying to search image !{}!'.format(text))
    if re.search(' ' + selfnames, text):
        return ('Replyimg', './data/pizdun.jpg')

    links = google_result(text)
    if isinstance(links, list):
        logger.info('Result links {}'.format(links))
        if len(links) == 0:
            return 'Reply', 'Зеро картинок. Попробуй по-другому'
        return 'Replyimg', random.choice(links)
    logger.warning('Google search error {}'.format(links))
    return 'Reply', links
