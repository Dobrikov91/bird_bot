''' information: https://docs.djangoproject.com/en/3.0/topics/logging/ \n
    DEBUG (10): Low level system information for debugging purposes\n
    INFO (20): General system information\n
    WARNING (30): Information describing a minor problem that has occurred\n
    ERROR (40): Information describing a major problem that has occurred\n
    CRITICAL (50): Information describing a critical problem that has occurred\n'''

import os
import logging
import logging.config

LOGGER_CONFIG = { 
    'version': 1,
    #'disable_existing_loggers': True,

    'formatters': {
        'standard': { 
            'format': '%(asctime)s|%(levelname)8s|%(filename)15s:%(name)15s:%(lineno)3s|%(funcName)20s()|%(message)s'
        }
    },

    'handlers': {
        'debug': { 
            'formatter': 'standard',
            #'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },        
        'info': { 
            'formatter': 'standard',
            #'level': 'INFO',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
        'file': {
            'formatter': 'standard',
            #'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': './out.log',  # File output
        }
    },
    
    'loggers': { 
        '': {  # base logger
            'handlers': ['file', 'info'],
            'level': 'INFO', 
            #'propagate': False
        },
        '__main__': {
            'handlers': ['debug',],
            'level': 'DEBUG', 
            #'propagate': False
        }
    } 
}

logging.config.dictConfig(LOGGER_CONFIG)
