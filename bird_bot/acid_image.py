import copy
import random
import math
import logging

from PIL import Image
import numpy

from bird_bot import logger_config

logger = logging.getLogger(__name__)

# pixel -> [index_in_column, [R,G,B]]
def process_pixel(pixel, i, koef):
    j = pixel[0]
    return list(map(lambda color: int(((koef + i + j) / koef * color)) & 255, pixel[1]))
# new_data[i][j][k] = ((val + i + j) / (400 * math.sin(i / 100) + 1) * new_data[i][j][k]) % 256

# column -> [index in row, [pixels]]
def process_column(column, koef):
    return list(map(process_pixel,
                    enumerate(column[1]),
                    [column[0]] * len(column[1]),
                    koef))

'''
def process_image(rows, koef):
    return numpy.asarray(list(map(process_column, enumerate(rows), koef)), dtype=numpy.uint8)
'''

def process_image2(image, koef):
    res = [[[((koef + i + j) * image[i][j][k] // koef)
             for k in [0, 1, 2]]
            for j in range(len(image[0]))]
           for i in range(len(image))]
    return numpy.asarray(res, dtype=numpy.uint8)

def process_file_image(file_path, value, new_file_path):
    data = numpy.array(Image.open(file_path))
    data = process_image2(data, value)
    image = Image.fromarray(data)
    #image.show()
    image.save(new_file_path)

def get_acid_image(text, file_path):
    if not file_path:
        return 'Reply', 'Где фото?'
    logger.debug('Go process image')

    value = 0
    for word in text.split(' '):
        try:
            value = int(word)
        except ValueError:
            pass
    value = 200 if value == 0 else value

    logger.debug('Current value {}'.format(value))
    new_file_path = file_path.split('.')
    new_file_path = '.' + new_file_path[1] + 'res.' + new_file_path[2]
    logger.debug('New image path {}'.format(new_file_path))
    process_file_image(file_path, value, new_file_path)
    return 'Replyimg', new_file_path
