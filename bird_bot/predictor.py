'''
https://yandex.ru/dev/predictor/doc/dg/reference/complete-docpage/
'''

import os
import requests
import json
import logging
import random

from bird_bot import logger_config

logger = logging.getLogger(__name__)

def get_url(text):
    return 'https://predictor.yandex.net/api/v1/predict.json/complete?key={}&q={}&lang={}'.format(
        os.getenv('BIRD_YANDEX_PREDICTOR_KEY'),
        text,
        'ru',
    )

prepositions = ('в', 'без', 'до', 'из', 'к', 'на', 'по', 'о', 'от', 'перед', 'при', 'через', 'с', 'у', 'за', 'над', 'об', 'под', 'про', 'для')
unions = ('и', 'а', 'но', 'да', 'что', 'если', 'когда')

def get_phrase(text):
    used_words = []

    max_len = random.randrange(5, 77)
    for i in range(1, max_len): # limit max iterations
        resp = requests.get(get_url(text))
        if resp.status_code != 200:
            return 'Reply', 'Сочинялка поломалась(( Код {}'.format(resp.status_code)

        logger.debug(resp.text)
        data = json.loads(resp.text)

        if 'text' not in data.keys():
            return 'Reply', text

        position, word = data['pos'], data['text'][0]
        if word in prepositions:
            if random.randrange(0, 1) == 0:
                logger.info('prep replaced')
                word = random.choice(prepositions)
        elif word in unions:
            if random.randrange(0, 1) == 0:
                logger.info('union replaced')
                word = random.choice(unions)
        else:
            if len(word) > 5 and word in used_words:
                # magic delete last word cos it can be related with used
                text = text.rsplit(' ', 1)[0] 
                return 'Reply', text
            used_words.append(word)

        if position == 1:
            text = text + ' ' + word
        else:
            text = text[:position] + word
        logging.debug('Temp response {}: {}'.format(i, text))
    return 'Reply', text
