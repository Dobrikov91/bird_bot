import random
import logging

from bird_bot import logger_config

logger = logging.getLogger(__name__)

path = './data/books/'
books = ('vim.txt','vechera.txt', 'prestup.txt', 'bible.txt',)

def get_divination(text):
    logger.debug('Get divination {}'.format(text))
    name = books[random.randrange(len(books))]
    file = open(path + name, 'r')
    text = file.readlines()
    file.close()
    
    x = random.randrange(len(text))
    # skip small parts
    while len(text[x]) < 20:
        x = random.randrange(len(text))
    logger.debug('Divination {}'.format(text[x]))
    return 'Reply', text[x]
