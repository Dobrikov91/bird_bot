# DEPRECATED API, NOT USED
'''
https://yandex.ru/dev/translate/doc/dg/reference/translate-docpage/
'''

import os
import requests
import json
import random
import logging

class Translator():
    def __init__(self, max_iterations):
        self.logger = logging.getLogger(__name__)
        self.api_key = os.getenv('BIRD_YANDEX_TRANSLATE_KEY')
        if not self.api_key:
            self.logger.warning('Yandex translate api key (BIRD_YANDEX_TRANSLATE_KEY) isnt specified. Feature not working')
            return
        self.langs = []
        self.doubles = []
        self.max_iterations = max_iterations
        
        url = 'https://translate.yandex.net/api/v1.5/tr.json/getLangs?key={}&ui={}'.format(
            os.getenv('BIRD_YANDEX_TRANSLATE_KEY'),
            'ru')
        resp = requests.get(url)
        if resp.status_code != 200:
            raise Exception('Wrong response from ya.translator {}'.format(resp.status_code))

        data = json.loads(resp.text)
        self.langs = list(data['langs'].keys())
        self.logger.debug('Langs: {}'.format(self.langs))
    
        for x in self.langs:
            for y in self.langs:
                if '{}-{}'.format(x, y) in data['dirs'] and '{}-{}'.format(y, x) in data['dirs']:
                    if x not in self.doubles:
                        self.doubles.append(x)
                    if y not in self.doubles:
                        self.doubles.append(y)
        self.logger.debug('Translator instance created\nDual-way langs: {}'.format(self.doubles))
    
    def get_url(self, text, lang):
        return 'https://translate.yandex.net/api/v1.5/tr.json/translate?key={}&text={}&lang={}&format={}'.format(
            os.getenv('BIRD_YANDEX_TRANSLATE_KEY'),
            text,
            lang,
            'plain',
        )

    def translate(self, text, lang):
        resp = requests.get(self.get_url(text, lang))
        if resp.status_code != 200:
            raise Exception('Wrong response from ya.translator {}'.format(resp.status_code))
        data = json.loads(resp.text)
        self.logger.debug('Translation {}'.format(data['text'][0]))
        return data['text'][0]

    def multitranslate(self, text):
        self.logger.debug('Initial phrase {}'.format(text))
        last_lang = 'ru'
        
        iters = random.randrange(self.max_iterations // 2, self.max_iterations)
        while iters:
            new_lang = 'ru'
            while new_lang == last_lang or new_lang == 'ru':
                new_lang = random.choice(self.doubles)
            text = self.translate(text, '{}-{}'.format(last_lang, new_lang))
            last_lang = new_lang
            iters -= 1
        text = self.translate(text, '{}-ru'.format(last_lang))
        self.logger.debug('Result phrase {}'.format(text))
        return text

    def get_multitranslate(self, text):
        result = self.multitranslate(text) + '\n\nПереведено сервисом «Яндекс.Переводчик»\nhttp://translate.yandex.ru'
        return 'Reply', result
