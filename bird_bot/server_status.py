import subprocess
from subprocess import check_output
import logging

from bird_bot import logger_config

logger = logging.getLogger(__name__)

def get_server_status(text):
    logger.debug('Trying to get server status {}'.format(text))
    
    res = ''

    # CPU data
    output = check_output(['lscpu']).decode().split('\n')
    data = {}
    for line in output:
        if line:
            line = line.split(':')
            data[line[0]] = line[1]
    res = 'CPU {}\n'.format(data['Model name'].strip())

    # Memory usage
    output = check_output(['free', '-mh']).decode().split('\n')
    data = list(filter(None, output[1].split(' ')))
    res += 'RAM {}, used {}, free {}\n'.format(data[1], data[2], data[3])

    # HDD usage
    output = check_output(['df', '-h']).decode().split('\n')
    for line in output:
        data = list(filter(None, line.split(' ')))
        if data and data[-1] == '/':  # full hdd data
            res += 'HDD {}, free {} ({}%)\n'.format(data[1], data[3], 100 - int(data[4][:-1]))

    # uptime
    output = check_output(['uptime', '-p']).decode()
    res += output

    return 'Reply', res
