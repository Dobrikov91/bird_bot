# DEPRECATED API, NOT USED
 
import requests
import json
import random
import re
import logging

from bird_bot import logger_config

class Constitution():
    def __init__(self, translator):
        self.logger = logging.getLogger(__name__)
        self.translator = translator
        self.const_book = './data/books/const.txt'
        self.positions = []
        
        text = open(self.const_book, 'r')
        for line in text.readlines():
            if re.match(r'\d\..*', line):
                self.positions.append(line[3:].strip())
        text.close()
        self.logger.info('Constitution instance created')

    def get_replace(self, text):
        self.logger.debug('Const replace {}'.format(text))
        old_position = random.choice(self.positions)
        new_position = self.translator.get_multitranslate(old_position)[1]
        
        result = '{}\n\n===>>>\n\n{}'.format(old_position, new_position)
        return 'Reply', result
        