import logging
import re

from forex_python.converter import CurrencyRates
from forex_python.bitcoin import BtcConverter

from bird_bot import logger_config

logger = logging.getLogger(__name__)

coins = {'USD': r'(usd|доллар|бакс)',
         'EUR': r'(eur|евр)',
         'BTC': r'(btc|бит)',
        }
not_found = 'Где ты взял такие деньги?'

def get_prices(text):
    logger.debug('Try get prices {}'.format(text))
    for coin, name in coins.items():
        if re.search(name, text):
            logger.debug('Found {} coin'.format(coin))
            if coin == 'BTC':
                coin2 = 'USD'
                value = '%.2f' % BtcConverter().get_latest_price('USD')
            else:
                coin2 = 'RUB'
                value = '%.2f' % CurrencyRates().get_rates(coin)['RUB']
            response = '1 {} = {} {}'.format(coin, value, coin2)
            logger.info(response)
            return ('Reply', response)
    return ('Reply', not_found)
