import re
import random
from datetime import datetime
import logging

from telegram import Update, Message, Chat, User

from bird_bot.reply import reply
from bird_bot.currency import get_prices
from bird_bot.google_image import get_random_image
from bird_bot.divination import get_divination
from bird_bot.server_status import get_server_status
from bird_bot.acid_image import get_acid_image
from bird_bot.predictor import get_phrase
###
# Deprecated api, to remove to Yandex.Cloud
# from bird_bot.translator import Translator
# from bird_bot.constitution import Constitution

class Processing():
    def __init__(self, max_iterations):
        self.logger = logging.getLogger(__name__)
        # self.translator = Translator(max_iterations)
        # self.constitution = Constitution(self.translator)

        self.commands = {
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(show|покажи)\s':                     get_random_image,
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(say|погадай|скажи)\s':               get_divination,
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(pic|жми|фото)\s':                    get_acid_image,
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(price|курс)\s':                      get_prices,
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(status|server|статус)\s':            get_server_status,
            r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(predict|продолжи|дальше|сгенери)\s': get_phrase,
            # r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(translate|переведи)\s':              self.translator.get_multitranslate,
            # r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка).*\s+(change|измени|обнули)\s':            self.constitution.get_replace,
            }

        self.command_reply = r'(\A|\s+)(pizdun|bird|birb|пиздун|попугай|попка)'
        self.logger.info('Processing instance created')

    def find_command(self, text):
        for command, function in self.commands.items():
            found = re.search(command, text, re.IGNORECASE)
            if found:
                return (text[found.span()[1]:]).strip(), function
        if re.search(self.command_reply, text, re.IGNORECASE):
            return '', reply
        return None

    def process_message(self, bot, update):
        self.logger.debug('message text={} caption={}'.format(
                        (update.message.text if update.message.text else '~'),
                        (update.message.caption if update.message.caption else '~')))

        # check body with caption
        text = ('{} {}'.format(update.message.text if update.message.text else ' ', 
                update.message.caption if update.message.caption else ''))
    
        if self.find_command(text) is not None:
            new_text, function = self.find_command(text)
            if function in (get_random_image,
                            get_divination, 
                            get_prices, 
                            get_server_status, 
                            get_phrase,
                            ):
                return function(new_text)
            #if function in (self.translator.get_multitranslate, 
            #                self.constitution.get_replace, 
            #                ):
            #    return function(new_text)
            if function in (get_acid_image, ):
                if update.message.photo:
                    file_id = bot.getFile(update.message.photo[-1].file_id)
                    newFile = bot.get_file(file_id)
                    newFile.download('./data/tmp.jpg')
                    return function(new_text, './data/tmp.jpg')
                return function(new_text, None)
            if function in (reply, ):
                return function(text, update.message.from_user.first_name, update.message.chat.last_name)
        return None
