''' Telegram bot part

Для функций перевода используется сервис «Яндекс.Переводчик» http://translate.yandex.ru
'''

import logging
import sys
import os

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Message
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters

from bird_bot.processing import Processing
from bird_bot.help import help_message

class Bot():
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.processing = Processing(30)
        try:
            self.updater = Updater(token=os.getenv('BIRD_BOT_TOKEN'), use_context=True)
            self.logger.info('Initialization successful')
        except ValueError:
            self.logger.error('Bot token (BIRD_BOT_TOKEN env variable) not specified. Initialization failed.')
            raise

        self.bot = self.updater.bot
        self.dp = self.updater.dispatcher
        
        # add commands
        self.dp.add_handler(CommandHandler('start', self.start))
        self.dp.add_handler(CommandHandler('help', self.help))
        self.dp.add_handler(CommandHandler('stop', self.stop))
        self.dp.add_error_handler(self.error)
        # common messages
        self.dp.add_handler(MessageHandler(filters=Filters.update, callback=self.process_message))

    def run(self):
        # Start the Bot
        self.updater.start_polling()
        self.logger.info('Pooling was started')
        # Run the bot until the user presses Ctrl-C or the process receives SIGINT, SIGTERM or SIGABRT
        self.updater.idle()

    def start(self, update, context=None):
        self.logger.info('START command: {}'.format(update.message.text))
        update.message.reply_text('ПАЕХАЛИ', quote=True)

    def help(self, update, context=None):
        self.logger.info('HELP command {}'.format(update.message.text))
        update.message.reply_text(help_message, quote=True)
        
    def stop(self, update, context=None):
        self.logger.info('STOP command {}'.format(update.message.text))
        update.message.reply_text('CANT. STOP. ME. NOW)', quote=True)
    
    def error(self, update, context):
        """Log Errors caused by Updates."""
        self.logger.error('Update "{}" caused error "{}"'.format(update, context.error))

    def process_message(self, update, context=None):
        self.logger.debug('Incoming message {}'.format(update.message))
        try:
            result = self.processing.process_message(self.bot, update)
            if result:
                self.logger.debug('Result response {}'.format(result))
                if result[0] == 'Send':
                    update.message.reply_text(result[1], quote=False)
                elif result[0] == 'Reply':
                    update.message.reply_text(result[1], quote=True)
                elif result[0] == 'Replyimg':
                    self.logger.info(result[1])
                    if result[1][:4] != 'http':
                        update.message.reply_photo(photo=open(result[1], 'rb'), quote=True)
                    else:
                        update.message.reply_photo(photo=result[1], quote=True)
                else:
                    self.logger.warning('Wrong response {}'.format(result))
        except Exception as e:
            self.logger.error('Message processing crashed.', exc_info=True)
            update.message.reply_text('Ой, я уебался об стену\n{}'.format(e), quote=True)
