FROM ubuntu:latest

ADD /bird_bot/* /app/bird_bot/
ADD /data/pizdun.jpg /app/data/
ADD /data/books/* /app/data/books/
ADD requirements.txt /app

WORKDIR /app

# install python
RUN apt-get update
# Setup locale
RUN apt-get install -y locales
RUN locale-gen ru_RU.UTF-8
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# fix pytz installation
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get install python3 python3-pip -y
RUN pip3 install -r requirements.txt

CMD python3 -m bird_bot
