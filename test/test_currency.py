import unittest

from bird_bot.currency import get_prices

class TestCurrency(unittest.TestCase):
    def test_price(self):
        self.assertIsInstance(get_prices('btc'), tuple)
        self.assertRegex(get_prices('btc')[1], r'.*BTC.*')
        self.assertRegex(get_prices('usd')[1], r'.*USD.*')
        self.assertRegex(get_prices('ololo')[1], r'Где ты взял.*')
