from unittest import TestCase

from bird_bot.predictor import get_phrase

class TestPredictor(TestCase):
    def test_predictor(self):
        self.assertIsInstance(get_phrase('Тест фразы')[1], str)
        self.assertNotIn('Сочинялка', get_phrase('Тест фразы')[1], ) # no errors from predictor
        self.assertGreater(len(get_phrase('Тест фразы')[1].split(' ')), 6) # we got some new words
