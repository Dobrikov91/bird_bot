import unittest

from bird_bot.google_image import get_random_image

class TestGoogleImage(unittest.TestCase):
    def test_google_image(self):
        self.skipTest('This test could decrease daily limits of GoogleAPI serach')
        
        #print(google_request_url('cats'))
        #print(google_result('cats'))
        self.assertEqual(get_random_image('Dog')[0], 'Replyimg')
        self.assertEqual(get_random_image(' себя'), ('Replyimg', './data/pizdun.jpg'))
        self.assertIsInstance(get_random_image('Cat')[1], str)
        self.assertNotEqual(get_random_image(' отсебятина '), ('Replyimg', './data/pizdun.jpg'))
