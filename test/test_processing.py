import unittest
import re

from bird_bot.processing import Processing

class TestProcessing(unittest.TestCase):
    def test_commands(self):
        processing = Processing(1)

        self.assertEqual(processing.find_command('bird price btc')[0], 'btc')
        self.assertEqual(processing.find_command('  bird  price  AZAZ')[0], 'AZAZ')
        self.assertEqual(processing.find_command('Bird,,, price KEK')[0], 'KEK')
        self.assertEqual(processing.find_command('Dear  BIRD  price  AZAZ')[0], 'AZAZ')
        
        # books fortune
        self.assertEqual(processing.find_command('пиздун скажи как дела')[0], 'как дела')
        # currency
        self.assertEqual(processing.find_command('Дорогой пиздун! Какой курс бакса?')[0], 'бакса?')
        # image processing
        self.assertEqual(processing.find_command('Bird! ЖМИ 1000')[0], '1000')
        # image search, Wheel, Acid, prices, status
        self.assertEqual(processing.find_command('Пиздун, покажи котиков')[0], 'котиков')
        # reply
        self.assertEqual(processing.find_command('Пиздун!')[0], '')
        # reply
        self.assertEqual(processing.find_command('пиздун статус')[0], '')
        
        
        # wrong
        self.assertIsNone(processing.find_command('Пиждун'))
        self.assertIsNone(processing.find_command('Пипиздун'))
        self.assertIsNone(processing.find_command(' 123попка123'))

        self.assertEqual(processing.find_command('пиздун подскажи время')[0], '')
        self.assertEqual(processing.find_command('пиздун курск доллара')[0], '')
        self.assertEqual(processing.find_command('пиздун ЖМИ  ')[0], '')

    def test_process_message(self):
        self.processing = Processing(4)

        class FromUser():
            def __init__(self):
                self.first_name = 'Firstname'
        class Chat():
            def __init__(self):
                self.last_name = 'Lastname'
        class Message():
            def __init__(self, text):
                self.text = text
                self.caption = ''
                self.photo = None
                self.from_user= FromUser()
                self.chat = Chat()
        class Update():
            def __init__(self, text):
                self.message = Message(text)
        '''
        update = Update('Пиздун жми 1000')
        self.assertEqual(self.processing.process_message(None, update)[1], 'Где фото?')

        update = Update('Пиздун курс бакса')
        self.assertIsInstance(self.processing.process_message(None, update), tuple)
        self.assertNotEqual(self.processing.process_message(None, update)[1].find('USD'), -1)
        
        update = Update('Пиздун скажи как нам быть')
        self.assertIsInstance(self.processing.process_message(None, update)[1], str)
        
        update = Update('Пиздун, пожалуйста покажи котиков!')
        self.assertIsInstance(self.processing.process_message(None, update)[1], str)
        
        update = Update('Пиздун!')
        self.assertIsInstance(self.processing.process_message(None, update)[1], str)

        update = Update('Bird status')
        self.assertIsInstance(self.processing.process_message(None, update)[1], str)

        update = Update('Пиздун продолжи как дела')
        self.assertIsInstance(self.processing.process_message(None, update)[1], str)
        '''
        
        update = Update('Пиздун переведи Как насчет попить кофе вечером,')
        result = self.processing.process_message(None, update)
        print(result)
        self.assertIsInstance(result[1], str)

        update = Update('Пиздун измени')
        result = self.processing.process_message(None, update)
        print(result)
        self.assertIsInstance(result[1], str)
