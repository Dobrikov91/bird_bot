import unittest

from PIL import Image
import numpy

from bird_bot.acid_image import get_acid_image

class TestAcidImage(unittest.TestCase):
    def test_get_acid_image(self):
        self.assertEqual(get_acid_image('50', './data/pizdun.jpg')[0], 'Replyimg')
        self.assertEqual(get_acid_image('haha', None)[0], 'Reply')

    def run_acid_image(self):
        get_acid_image('50', './data/pizdun.jpg')
        data = numpy.array(Image.open('./data/pizdunres.jpg'))
        image = Image.fromarray(data)
        image.show()
        
        
        get_acid_image('   100', './data/pizdun.jpg')
        data = numpy.array(Image.open('./data/pizdunres.jpg'))
        image = Image.fromarray(data)
        image.show()
        
        get_acid_image(' 200', './data/pizdun.jpg')
        data = numpy.array(Image.open('./data/pizdunres.jpg'))
        image = Image.fromarray(data)
        image.show()
