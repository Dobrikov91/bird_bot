import unittest

from bird_bot.divination import get_divination

class TestDivination(unittest.TestCase):
    def test_divination(self):
        self.assertEqual(get_divination('')[0], 'Reply')
        self.assertIsInstance(get_divination('')[1], str)
